import swal from 'sweetalert';
export const mensajes = (texto,type='success',title='Exitoso') => swal({
    title: title,
    text: texto,
    icon: type,
    button: 'OK',
    timer: 3000

});
 
export default mensajes;