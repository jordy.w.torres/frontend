const URL = "http://localhost:3006/api"
export const InicioSesion = async (data) => {
    
    const headers = {
        'Accep':'application/json',
        'Content-Type':'application/json'
    }
    const datos = await (await fetch(URL + "/sesion", {
        method: "POST",
        headers:headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}

/*export const Marcas = async (key) => {
    var cabeceras = { "X-API-KEY": key };
    const datos = await (await fetch(URL + "/marcas", {
        method: "GET",
        headers: cabeceras
    })).json();
    return datos;
}
/*INSERT INTO marca (nombre, modelo, paisOrigen, external_id, estado, createdAt, updatedAt)
VALUES ('Nike', 'Air Max', 'Estados Unidos', '123456', 'Activo', '2023-06-21', '2023-06-21');
*/
export const MarcasCantidad = async () => {
  try {
    const response = await fetch(URL+'/marcas/cantidad');
    if (!response.ok) {
      throw new Error('Error al obtener la cantidad de marcas');
    }
    const data = await response.json();
    console.log(data);
    return data;
  } catch (error) {
    console.error(error);
    // Manejar el error de alguna manera adecuada en tu aplicación
    return null;
  }
};

export const AutosCantidad = async () => {
  try {
    const response = await fetch(URL+'/autos/cantidad');
    if (!response.ok) {
      throw new Error('Error al obtener la cantidad de autos');
    }
    const data = await response.json();
    console.log(data);
    return data;
  } catch (error) {
    console.error(error);
    // Manejar el error de alguna manera adecuada en tu aplicación
    return null;
  }
};
export const obtenerAutos = async () => {
    try {
      const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
      const respuesta = await fetch(URL + "/autos/listar", {
        method: "GET",
        headers: cabeceras
      });
  
      if (!respuesta.ok) {
        throw new Error("Error al obtener los datos de autos");
      }
  
      const datos = await respuesta.json();
      return datos.info; // Solo devolvemos la propiedad "info" de la respuesta
  
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  export const obtenerMarcas = async () => {
    try {
      const cabeceras = { "X-API-KEY": "token" }; // Reemplaza "tu_token" con el valor de tu token
      const respuesta = await fetch(URL + "/marcas/listar", {
        method: "GET",
        headers: cabeceras
      });
  
      if (!respuesta.ok) {
        throw new Error("Error al obtener los datos de autos");
      }
  
      const datos = await respuesta.json();
      return datos.info; // Solo devolvemos la propiedad "info" de la respuesta
  
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  export const GuardarAuto = async (data, key) => {
    console.log(data);
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/autos/guardar", {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}

export const modificarAutos = async (data, key) => {
    console.log(data);
    const headers = {
        'Accept': 'application/json',
        "Content-Type": "application/json",
        "X-API-TOKEN": key        
    };
    const datos = await (await fetch(URL + "/autos/modificar", {
        method: "PUT",
        headers: headers,
        body: JSON.stringify(data)
    })).json();
    return datos;
}
  