import Sesion from './fragment/Sesion';
import { Routes, Route, useLocation, Navigate } from 'react-router-dom';
import Principal from './fragment/Principal';
import Inicio from './fragment/Inicio';
import Crud from './fragment/Crud';
import { estaSesion } from './utilidades/Sessionutil';
function App() {
  const Middeware = ({children}) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if (autenticado) {
      return children;
    } else {
      return <Navigate to= '/sesion' state={location}/>;
    }
  }
  const MiddewareSesion = ({children}) => {
    const autenticado = estaSesion();
    const location = useLocation();
    if (autenticado) {
      return <Navigate to= '/inicio' />;
    } else {
      return children;
    }
  }
  return (
    <div className="App">
      <Routes>
        <Route path='/' element={<MiddewareSesion><Principal /></MiddewareSesion>} exact />
        <Route path='/sesion' element={<MiddewareSesion><Sesion /></MiddewareSesion>} />
        <Route path='/inicio' element={<Middeware><Inicio/></Middeware>}/>
        <Route path='/auto' element={<Middeware><Crud/></Middeware>} />
      </Routes>
    </div>
  );
  
}
export default App;