import '../css/stylea.css';
import Header from "./Header";
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import { GuardarAuto, obtenerMarcas} from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useForm } from 'react-hook-form';
const RegistroAutos = () => {
    const navegation = useNavigate();
    const { register, handleSubmit, formState: { errors } } = useForm();
    const [marcas, setMarcas] = useState([]);
    const [llmarca, setLlmarca] = useState(false);
    //acciones
    //submit
    const onSubmit = (data) => {
        var datos = {
            "external_marca": data.marca,
            "modelo": data.modelo,
            "costo": data.costo,
            "placa": data.placa,
            "kilometraje": data.kilometraje,
            "color": data.color
        };
        GuardarAuto(datos, getToken()).then((info) => {
            if (info.code != 200) {
                //console.log(info);
                mensajes(info.msg, 'error', 'Error');
                //msgError(info.message);            
            } else {
                mensajes(info.msg);
                navegation('/auto');
            }
        }
        );
    };

    if (!llmarca) {
        obtenerMarcas(getToken()).then((info) => {
            //console.log(info);
            if (info.error === true && info.message == 'Acceso denegado. Token ha expirado') {
                borrarSesion();
                mensajes(info.message);
                navegation("/sesion");
            } else {
                console.log(info.info);
                setMarcas(info.info);
                setLlmarca(true);
            }
        });
    }
    return (
        <div className="wrapper">
            <div className="d-flex flex-column">
                <div className="content">
               
                    {/** DE AQUI CUERPO */}

                    <div className='container-fluid'>
                        <div className="col-lg-10">
                            <div className="p-5">
                                <div className="text-center">
                                    <h1 className="h4 text-gray-900 mb-4">Registro de autos!</h1>
                                </div>
                                <form className="user" onSubmit={handleSubmit(onSubmit)}>
                                    <div className="form-group">
                                        <input type="text" {...register('modelo', { required: true })} className="form-control form-control-user" placeholder="Ingrese el modelo" />
                                        {errors.modelo && errors.modelo.type === 'required' && <div className='alert alert-danger'>Ingrese un modelo</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el costo" {...register('costo', { required: true })} />
                                        {errors.costo && errors.costo.type === 'required' && <div className='alert alert-danger'>Ingrese el costo</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el placa" {...register('placa', { required: true })} />
                                        {errors.placa && errors.placa.type === 'required' && <div className='alert alert-danger'> Por favor, proporcione la placa del auto.</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el color" {...register('color', { required: true })} />
                                        {errors.color && errors.color.type === 'required' && <div className='alert alert-danger'> Por favor, proporcione el color del auto.</div>}
                                    </div>
                                    <div className="form-group">
                                        <input type="text" className="form-control form-control-user" placeholder="Ingrese el kilometraje" {...register('kilometraje', { required: true, pattern: /^[0-9]*(\.[0-9]{0,2})?$/ })} />
                                        {errors.kilometraje && errors.kilometraje.type === 'required' && <div className='alert alert-danger'>Por favor, proporcione un kilometraje.</div>}
                                        {errors.kilometraje && errors.kilometraje.type === 'pattern' && <div className='alert alert-danger'>Ingrese un kilometraje valido</div>}
                                    </div>
                                    <div className="form-group">
                                        <select className='form-control' {...register('marca', { required: true })}>
                                            <option>Elija una marca</option>
                                            {marcas.map((m, id) => {
                                                return (<option key={id} value={m.external_id}>
                                                    {m.nombre}
                                                </option>)
                                            })}
                                        </select>
                                        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'>Selecione una marca</div>}

                                    </div>
                                    <hr />
                                    <input className="btn btn-facebook btn-user btn-block" type='submit' value="REGISTRAR"></input>
                                </form>

                                <hr />

                                <div className="text-center">
                                    <a className="small" href="register.html">Create an Account!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
}

export default RegistroAutos;