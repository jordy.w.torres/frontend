import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { useNavigate } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import { useForm } from 'react-hook-form';
import { obtenerMarcas, modificarAutos, GuardarAuto } from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
function RegistroAuto() {
  const navegation = useNavigate();
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [validated, setValidated] = useState(false);

  const [marca, setMarca] = useState('');
  const [marcas, setMarcas] = useState([]);

  useEffect(() => {
    obtenerListaMarcas();
  }, []);

  const obtenerListaMarcas = async () => {
    try {
      const marcas = await obtenerMarcas();
      console.log(marcas);
      setMarcas(marcas);
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  const onSubmit = (data) => {
    console.log(data);

    var datos = {
      "external_id": data.marca,
      "modelo": data.modelo,
      "costo": data.costo,
      "placa": data.placa,
      "kilometraje": data.kilometraje,
      "color": data.color,
      "tipoCombustible":data.tipoCombustible,
      "numeroPuertas":data.numeroPuertas,
      "dni_duenio":data.dni_duenio
        };
    GuardarAuto(datos, getToken()).then((info) => {
      console.log(info);
      if (info.code != 200) {
        //console.log(info);
        mensajes(info.msg, 'error', 'Exitoso');
        //msgError(info.message);            
      } else {
        mensajes(info.msg);
        navegation('/auto');
      }
    }
    );
  };

  return (
    <form className="user" onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <input type="text" {...register('modelo', { required: true })} className="form-control form-control-user" placeholder="Ingrese el modelo" />
        {errors.modelo && errors.modelo.type === 'required' && <div className='alert alert-danger'>Ingrese un modelo</div>}
      </div>
      <div className="form-group">
        <input type="text" className="form-control form-control-user" placeholder="Ingrese el costo" {...register('costo', { required: true })} />
        {errors.costo && errors.costo.type === 'required' && <div className='alert alert-danger'>Ingrese el costo</div>}
      </div>
      <div className="form-group">
        <input type="text" className="form-control form-control-user" placeholder="Ingrese el placa" {...register('placa', { required: true })} />
        {errors.placa && errors.placa.type === 'required' && <div className='alert alert-danger'> Por favor, proporcione la placa del auto.</div>}
      </div>
      <div className="form-group">
        <input type="text" className="form-control form-control-user" placeholder="Ingrese el numero de Puertas" {...register('numeroPuertas', { required: true })} />
        {errors.numeroPuertas && errors.numeroPuertas.type === 'required' && <div className='alert alert-danger'> Por favor, proporcione el numero de puertas.</div>}
      </div>
      <div className="form-group">
        <input type="text" className="form-control form-control-user" placeholder="Ingrese el Tipo de Combustible" {...register('tipoCombustible', { required: true })} />
        {errors.tipoCombustible && errors.tipoCombustible.type === 'required' && <div className='alert alert-danger'> Por favor, proporcione el tipo de combustible</div>}
      </div>
      <div className="form-group">
        <input type="text" className="form-control form-control-user" placeholder="Ingrese el color" {...register('color', { required: true })} />
        {errors.color && errors.color.type === 'required' && <div className='alert alert-danger'> Por favor, proporcione el color del auto.</div>}
      </div>
      <div className="form-group">
        <input type="text" className="form-control form-control-user" placeholder="Ingrese el kilometraje" {...register('kilometraje', { required: true, pattern: /^[0-9]*(\.[0-9]{0,2})?$/ })} />
        {errors.kilometraje && errors.kilometraje.type === 'required' && <div className='alert alert-danger'>Por favor, proporcione un kilometraje.</div>}
        {errors.kilometraje && errors.kilometraje.type === 'pattern' && <div className='alert alert-danger'>Ingrese un kilometraje valido</div>}
      </div>
      <div className="form-group">
        <input type="text" className="form-control form-control-user" placeholder="Ingrese el dni" {...register('dni_duenio', { required: false,default:"NO_DATA"})} />
         </div>
      <div className="form-group">
        <select className='form-control' {...register('marca', { required: true })}>
          <option>Elija una marca</option>
          {marcas.map((m, id) => {
            return (<option key={id} value={m.external_id}>
              {m.nombre}
            </option>)
          })}
        </select>
        {errors.marca && errors.marca.type === 'required' && <div className='alert alert-danger'>Selecione una marca</div>}

      </div>
      <hr />
      {' '}
      <Button className="btn btn-success mt-4" type="submit" >Guardar auto</Button>
    </form>

  );
}

export default RegistroAuto;
